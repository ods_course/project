FROM python:3.11-slim

WORKDIR /app

RUN pip install poetry==1.8.2

COPY poetry.lock pyproject.toml README.md ./

ARG MODE="prod"

RUN poetry config virtualenvs.create false \
    && rm -rf $(poetry config cache-dir)/{cache,artifacts} \
    && if [ "$MODE" = "dev" ]; then poetry install; else poetry install --without dev; fi

COPY src ./src

COPY docs/_site/ ./docs/_site
