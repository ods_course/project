# Линтеры и форматтеры
В проекте используются `Ruff` в качестве линтера и форматтера, `MyPy` для статической проверки типов и `pre-commit` для автоматизации проверок перед коммитами. Хуки для `pre-commit` прописаны в файле `.pre-commit-config.yaml`, настройки `Ruff` и `MyPy` - в `pyproject.toml`.

## Установка
С помощью dev-окружения `Poetry`, либо через `pip`:
```bash
pip install pre-commit
pip install ruff
pip install mypy
```

## Применение
Запускается автоматически при сохранении и перед коммитами. Также прописан отдельный стейдж для линтинга в Gitlab CI-CD.

Ручной запуск
```bash
mypy .
ruff check
ruff format
```

# Документация

Для сборки исследований и документации кода используется `Quarto` с расширением `quartodoc`.

## Установка
```bash
pip install -U quarto-cli jupyter
pip install quartodoc
```

## Применение
```bash
quarto create project website --no-open "docs"
quartodoc build --config="docs/_quarto.yml"
quarto render {path}/{file}.ipynb
quarto render docs
quarto preview docs
```
