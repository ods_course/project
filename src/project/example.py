import pandas as pd

# a = pd.DataFrame({'c0': [4], 'c1': [5.6]})
# b = 1
# c = 2.3
# d = 'test'


def example(a: pd.DataFrame, b: int, c: float, d: str) -> pd.DataFrame:
    """
    Тестовая функция

    Args:
        a (pd.DataFrame): тестовый датафрейм
        b (int): тестовая перемнная
        c (float): тестовая переменная
        d (str): тестовая строка

    Returns:
        pd.DataFrame: итоговый датафрейм
    """

    x = pd.concat([a, pd.DataFrame({'c0': [b], 'c1': [c]})], axis=0).rename(columns={'c0': d})
    return x


# print(example(a, b, c, d))
