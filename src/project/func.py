import pandas as pd
import xgboost as xgb
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
import joblib


def read(input_path: str, output_path: str) -> None:
    """
    Чтение и очистка датасета

    Args:
        input_path (str): путь к входному файлу
        output_path (str): путь к выходному файлу
    """

    df = pd.read_csv(input_path)
    df = df[[i for i in df.columns if 'feature' in i] + ['target']]
    df = df.select_dtypes(include=['float', 'int'])
    df = df.dropna(thresh=df.shape[0] * 0.25, axis=1)
    df = df.dropna(subset=['target'])
    df = df.drop_duplicates()
    df.to_csv(output_path, index=False)


def preprocess_corr(input_path: str, output_path: str, corr_threshold: float) -> None:
    """
    Предобработка датасета - удаление коррелирующих признаков

    Args:
        input_path (str): путь к входному файлу
        output_path (str): путь к выходному файлу
        corr_threshold (float): пороговая доля пропусков для признаков
    """

    df = pd.read_csv(input_path)

    features = [i for i in df.columns if 'feature' in i]

    corr_matrix = df[features].corr().abs()

    feature_stat = {
        i: {'nan_count': df[i].isnull().sum(), 'corr_sum': corr_matrix[i].sum()} for i in features
    }

    features_to_drop = set()
    for i in range(corr_matrix.shape[1]):
        for j in range(i + 1, corr_matrix.shape[1]):
            col1, col2 = corr_matrix.columns[i], corr_matrix.columns[j]
            if (col1 in features_to_drop) | (col2 in features_to_drop):
                continue
            if corr_matrix.iloc[i, j] > corr_threshold:
                if (feature_stat[col1]['nan_count'] < feature_stat[col2]['nan_count']) | (
                    (feature_stat[col1]['nan_count'] == feature_stat[col2]['nan_count'])
                    & (feature_stat[col1]['corr_sum'] < feature_stat[col2]['corr_sum'])
                ):
                    features_to_drop.add(col2)
                else:
                    features_to_drop.add(col1)

    df = df.drop(list(features_to_drop), axis=1)
    df.to_csv(output_path, index=False)


def preprocess_fillna(input_path: str, output_path: str, method: str) -> None:
    """
    Предобработка датасета - заполнение пропусков медианным значением

    Args:
        input_path (str): путь к входному файлу
        output_path (str): путь к выходному файлу
        method (str): способ заполнения пропусков, должен принимать значения 'median' или 'mean'
    """

    df = pd.read_csv(input_path)

    if method == 'median':
        df = df.fillna(df.median())
    elif method == 'mean':
        df = df.fillna(df.mean())

    df.to_csv(output_path, index=False)


def train(
    input_path: str, output_path: str, model: str, xgb_params: dict, lr_params: dict
) -> None:
    """
    Обучение 2 вариантов моделей: XGBoostClassifier и LogisticRegression

    Args:
        input_path (str): путь к входному файлу
        output_path (str): путь к выходному файлу
        model (str): вариант модели, должен принимать значения 'xgb' или 'lr'
    """

    df = pd.read_csv(input_path)

    X = df.drop('target', axis=1)
    Y = df['target']

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=1)

    if model == 'xgb':
        params = xgb_params
        model = xgb.XGBClassifier()
    elif model == 'lr':
        params = lr_params
        model = LogisticRegression(max_iter=1000)

    grid_search = GridSearchCV(model, params, scoring='roc_auc', cv=4)
    grid_search.fit(X_train, Y_train)
    Y_pred = grid_search.predict_proba(X_test)[:, 1]

    print('Best Parameters:', grid_search.best_params_)
    print('Cross-Validation ROC AUC Score:', grid_search.best_score_)
    print('Test ROC AUC Score:', roc_auc_score(Y_test, Y_pred))

    joblib.dump(grid_search.best_estimator_, output_path)
