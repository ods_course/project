from hydra import compose, initialize

initialize(version_base=None, config_path='/conf')
cfg = compose(config_name="config.yaml")

PREPROCESSING = ['corr', 'fillna']
MODEL = ['xgb', 'lr']

rule all:
    input:
        expand('artifacts/model_{p}_{m}.pkl', p=PREPROCESSING, m=MODEL),


rule read:
    input:
        'data/dataset.csv',
    output:
        'data/dataset_read.csv',
    run:
        from project.func import read
        read(input[0], output[0])

rule preprocess_corr:
    input:
        'data/dataset_read.csv',
    output:
        'data/dataset_preprocess_corr.csv',
    run:
        from project.func import preprocess_corr
        preprocess_corr(input[0], output[0], cfg['preprocessing']['corr'])

rule preprocess_fillna:
    input:
        'data/dataset_read.csv',
    output:
        'data/dataset_preprocess_fillna.csv',
    run:
        from project.func import preprocess_fillna
        preprocess_fillna(input[0], output[0], cfg['preprocessing']['fillna'])

rule train:
    input:
        'data/dataset_preprocess_{p}.csv',
    output:
        'artifacts/model_{p}_{m}.pkl',
    run:
        from project.func import train
        train(input[0], output[0], '{m}', cfg['training']['xgb_params'], cfg['training']['lr_params'])
